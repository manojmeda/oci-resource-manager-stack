resource "oci_core_vcn" "test_vcn" {
  cidr_block     = "10.0.0.0/16"
  dns_label      = "testvcn"
  compartment_id = "${var.compartment_ocid}"
  display_name   = "testvcn-${var.suffix}"
}